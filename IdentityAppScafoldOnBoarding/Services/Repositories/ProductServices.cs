﻿using AutoMapper;
using IdentityAppScafoldOnBoarding.Data;
using IdentityAppScafoldOnBoarding.Models;
using IdentityAppScafoldOnBoarding.Services.Contracts;
using IdentityAppScafoldOnBoarding.ViewModels;

namespace IdentityAppScafoldOnBoarding.Services.Repositories
{
    public class ProductServices : IProductServices
    {
        private DataContext _context;
        private readonly IMapper _mapper;
        public ProductServices(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public ICollection<Product> GetProducts()
        {
            return _context.Products.ToList();
        }

        public Product GetProduct(int id)
        {
            return _context.Products.Where(e => e.Id == id).FirstOrDefault();
        }

        public bool CreateProduct(Product product)
        {
            _context.Add(product);
            return Save();
        }

        public bool UpdateProduct(Product product)
        {
            _context.Update(product);
            return Save();
        }

        public bool DeleteProduct(Product product)
        {
            _context.Remove(product);
            return Save();
        }

        public bool ProductExists(int id)
        {
            return _context.Products.Any(c => c.Id == id);
        }

        public bool Save()
        {
            var saved = _context.SaveChanges();
            return saved > 0 ? true : false;
        }
    }
}
