﻿using IdentityAppScafoldOnBoarding.Models;
using IdentityAppScafoldOnBoarding.ViewModels;

namespace IdentityAppScafoldOnBoarding.Services.Contracts
{
    public interface IProductServices
    {
        ICollection<Product> GetProducts();
        public Product GetProduct(int id);
        bool ProductExists(int id);
        bool CreateProduct(Product product);
        bool UpdateProduct(Product product);
        bool DeleteProduct(Product product);
    }
}
