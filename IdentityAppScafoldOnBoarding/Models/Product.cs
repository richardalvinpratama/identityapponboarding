﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IdentityAppScafoldOnBoarding.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("CategoryId")]
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }
        public string Image { get; set; } = string.Empty;
        public int Stock { get; set; }
        public double Price { get; set; }
        public DateTime LastUpdate { get; set; }
        public Category Category { get; set; }
    }
}
