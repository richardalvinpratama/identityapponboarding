﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace IdentityAppScafoldOnBoarding.ViewModels
{
    public class ProductUpdateViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }
        public int Stock { get; set; }
        public double Price { get; set; }
        public int CategoryId { get; set; }
       /* public List<SelectList> CategoryLists { set; get; }*/
    }
}
