﻿using System.ComponentModel.DataAnnotations;

namespace IdentityAppScafoldOnBoarding.ViewModels
{
    public class CategoryViewModel
    {
        [Required]
        public string Name { get; set; }
    }
}
