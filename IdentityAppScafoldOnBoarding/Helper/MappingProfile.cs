﻿using AutoMapper;
using IdentityAppScafoldOnBoarding.Models;
using IdentityAppScafoldOnBoarding.ViewModels;

namespace IdentityAppScafoldOnBoarding.Helper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Product, ProductUpdateViewModel>();
            CreateMap<ProductUpdateViewModel, Product>();
        }
    }
}
