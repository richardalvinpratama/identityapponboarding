﻿using IdentityAppScafoldOnBoarding.Data;
using IdentityAppScafoldOnBoarding.Models;
using IdentityAppScafoldOnBoarding.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using PagedList;

namespace IdentityAppScafoldOnBoarding.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryServices _categoryServices;
        public CategoryController(ICategoryServices categoryServices)
        {
            _categoryServices = categoryServices;
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult IndexAdmin(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            var categories = _categoryServices.GetCategories();

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            if (!String.IsNullOrEmpty(searchString))
            {
                categories = categories.Where(s => s.Name.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "name_desc":
                    categories = categories.OrderByDescending(s => s.Name);
                    break;
                default:
                    categories = categories.OrderBy(s => s.Name);
                    break;
            }
            ViewBag.LinkText = "Category";
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(categories.ToPagedList(pageNumber, pageSize));
        }

        public IActionResult Create()
        {
            ViewBag.LinkText = "Category";
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Category obj)
        {
            _categoryServices.CreateCategory(obj);
            TempData["success"] = "Category Added successfully!";
            return RedirectToAction("IndexAdmin");
        }

        public IActionResult Update(int id)
        {
            ViewBag.LinkText = "Category";
            if (_categoryServices.CategoryExists(id))
            {
                return View(_categoryServices.GetCategory(id));
            }
            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(Category obj)
        {
            _categoryServices.UpdateCategory(obj);
            TempData["success"] = "Category update successfully!";
            return RedirectToAction("IndexAdmin");
        }

        public IActionResult Delete(int id)
        {
            if (_categoryServices.CategoryExists(id))
            {
                var category = _categoryServices.GetCategory(id);
                TempData["success"] = "Category delete successfully!";
                _categoryServices.DeleteCategory(category);
                return RedirectToAction("IndexAdmin");
            }
            return NotFound();
        }
    }
}
