﻿using IdentityAppScafoldOnBoarding.Models;
using IdentityAppScafoldOnBoarding.Services.Contracts;
using IdentityAppScafoldOnBoarding.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IdentityAppScafoldOnBoarding.Controllers
{
    public class ProductController : Controller
    {
        private readonly ICategoryServices _categoryServices;
        private readonly IProductServices _productServices;
        public ProductController(IProductServices productServices, ICategoryServices categoryServices)
        {
            _productServices = productServices;
            _categoryServices = categoryServices;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult IndexAdmin()
        {
            ViewBag.LinkText = "Product";
            return View(_productServices.GetProducts());
        }

        public IActionResult Detail(int id)
        {
            ViewBag.LinkText = "Product";
            var product = _productServices.GetProduct(id);
            return View(product);
        }

        public IActionResult Create()
        {
            ViewBag.LinkText = "Product";
            ViewBag.Categories = new SelectList(_categoryServices.GetCategories(), "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Product obj)
        {
            _productServices.CreateProduct(obj);
            TempData["success"] = "Product Added successfully!";
            return RedirectToAction("IndexAdmin");
        }

        public async Task<IActionResult> Update(int id)
        {
            ViewBag.LinkText = "Product";
            if (_productServices.ProductExists(id))
            {
                var product = _productServices.GetProduct(id);
                var productVM = new ProductUpdateViewModel
                {
                    Id = product.Id,
                    Name = product.Name,
                    Desc = product.Desc,
                    Stock = product.Stock,
                    Price = product.Price,
                    CategoryId = product.CategoryId
                    /*CategoryLists = new SelectList(_categoryServices.GetCategories(), "Id", "Name");*/
                };
                /*return View(_productServices.GetProduct(id));*/
                return View(productVM);
            }
            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(int id, ProductUpdateViewModel obj)
        {
            var product = new Product
            {
                Id = id,
                Name = obj.Name,
                Desc = obj.Desc,
                Stock = obj.Stock,
                Price = obj.Price,
                CategoryId = obj.CategoryId
            };
            _productServices.UpdateProduct(product);
            TempData["success"] = "Product update successfully!";
            return RedirectToAction("IndexAdmin");
        }

        public IActionResult Delete(int id)
        {
            if (_productServices.ProductExists(id))
            {
                var product = _productServices.GetProduct(id);
                TempData["success"] = "Product delete successfully!";
                _productServices.DeleteProduct(product);
                return RedirectToAction("IndexAdmin");
            }
            return NotFound();
        }
    }
}
