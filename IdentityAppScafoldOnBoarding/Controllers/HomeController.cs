﻿using IdentityAppScafoldOnBoarding.Models;
using IdentityAppScafoldOnBoarding.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace IdentityAppScafoldOnBoarding.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ICategoryServices _categoryServices;

        public HomeController(ILogger<HomeController> logger, ICategoryServices categoryServices)
        {
            _logger = logger;
            _categoryServices = categoryServices;
        }

        public IActionResult Index()
        {
            return View(_categoryServices.GetCategories());
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [Authorize]
        public IActionResult Admin()
        {
            ViewBag.LinkText = "AdminDashboard";
            return View();
        }

        public IActionResult ProductByCategory(int id)
        {
            return View(_categoryServices.GetProductByCategory(id));
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}