﻿using IdentityAppScafoldOnBoarding.Configuration;
using IdentityAppScafoldOnBoarding.Models;
using Microsoft.EntityFrameworkCore;

namespace IdentityAppScafoldOnBoarding.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CategoryConfiguration());
            modelBuilder.ApplyConfiguration(new ProductConfiguration());
        }

    }
}
