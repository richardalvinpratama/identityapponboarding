﻿using IdentityAppScafoldOnBoarding.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IdentityAppScafoldOnBoarding.Configuration
{
    public class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.ToTable("Category");
            builder.Property(c => c.Name)
                .IsRequired(true);

            builder.HasData
            (
                new Category
                {
                    Id = 1,
                    Name = "Sport",
                },
                new Category
                {
                    Id = 2,
                    Name = "Electronic",
                }
            );
        }
    }
}
